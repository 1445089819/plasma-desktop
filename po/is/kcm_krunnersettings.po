# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# gummi <gudmundure@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2023-03-12 22:08+0000\n"
"Last-Translator: gummi <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr "Staðsetning á skjá:"

#: ui/main.qml:33
#, kde-format
msgid "Top"
msgstr "Efst"

#: ui/main.qml:40
#, kde-format
msgid "Center"
msgstr "Miðja"

#: ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr "Virkjun:"

#: ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr "Virkja þegar ýtt er á einhvern lykil á skjáborðinu"

#: ui/main.qml:69
#, kde-format
msgid "History:"
msgstr "Ferill:"

#: ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr "Muna fyrri leitir"

#: ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr "Sýna síðustu leit þegar opnað er aftur"

#: ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr "Fylgist með athöfnum (síðasta leit og ferill)"

#: ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr "Hreinsa feril"

#: ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr "Hreinsa feril fyrir athöfnina \"%1\""

#: ui/main.qml:111
#, kde-format
msgid "Clear History…"
msgstr "Hreinsa feril..."

#: ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr "Fyrir allar athafnir"

#: ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr "Fyrir athöfnina \"%1\""

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr "Viðbætur:"

#: ui/main.qml:174
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Grunnstilla virkar leitarviðbætur..."
