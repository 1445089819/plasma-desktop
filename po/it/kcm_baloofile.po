# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2014, 2017.
# Paolo Zamponi <feus73@gmail.com>, 2017, 2019, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2022-12-14 19:03+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.0\n"

#: ui/main.qml:57
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Verrà disabilitata la ricerca dei file in KRunner e nei menu dei lanciatori, "
"e verrà rimossa la visualizzazione dei metadati estesi da tutte le "
"applicazioni di KDE."

#: ui/main.qml:66
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Vuoi eliminare i dati indice salvati? Verrebbero liberati %1 di spazio, ma "
"se l'indicizzazione verrà riabilitata successivamente, l'intero indice dovrà "
"essere ricreato da capo. Ci vorrà un po' di tempo, a seconda del numero di "
"file."

#: ui/main.qml:68
#, kde-format
msgid "Delete Index Data"
msgstr "Elimina i dati indice"

#: ui/main.qml:88
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr ""
"Il sistema deve essere riavviato affinché che queste modifiche abbiano "
"effetto."

#: ui/main.qml:92
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Riavvia"

#: ui/main.qml:99
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"La ricerca nei file ti aiuta a trovare facilmente tutti i tuoi file in base "
"al loro contenuto."

#: ui/main.qml:106
#, kde-format
msgid "Enable File Search"
msgstr "Abilita la ricerca nei file"

#: ui/main.qml:121
#, kde-format
msgid "Also index file content"
msgstr "Indicizza anche il contenuto dei file"

#: ui/main.qml:135
#, kde-format
msgid "Index hidden files and folders"
msgstr "Indicizza file e cartelle nascosti"

#: ui/main.qml:159
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Stato: %1, completo al %2%"

#: ui/main.qml:164
#, kde-format
msgid "Pause Indexer"
msgstr "Metti in pausa l'indicizzatore"

#: ui/main.qml:164
#, kde-format
msgid "Resume Indexer"
msgstr "Riprendi l'indicizzatore"

#: ui/main.qml:177
#, kde-format
msgid "Currently indexing: %1"
msgstr "Sto attualmente indicizzando: %1"

#: ui/main.qml:182
#, kde-format
msgid "Folder specific configuration:"
msgstr "Configurazione specifica per la cartella:"

#: ui/main.qml:209
#, kde-format
msgid "Start indexing a folder…"
msgstr "Inizia a indicizzare una cartella…"

#: ui/main.qml:220
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Interrompi l'indicizzazione di una cartella…"

#: ui/main.qml:271
#, kde-format
msgid "Not indexed"
msgstr "Non indicizzato"

#: ui/main.qml:272
#, kde-format
msgid "Indexed"
msgstr "Indicizzato"

#: ui/main.qml:302
#, kde-format
msgid "Delete entry"
msgstr "Elimina voce"

#: ui/main.qml:317
#, kde-format
msgid "Select a folder to include"
msgstr "Seleziona una cartella da includere"

#: ui/main.qml:317
#, kde-format
msgid "Select a folder to exclude"
msgstr "Seleziona una cartella da escludere"

#~ msgid ""
#~ "This module lets you configure the file indexer and search functionality."
#~ msgstr ""
#~ "Questo modulo ti permette di configurare l'indicizzatore dei file e le "
#~ "funzionalità di ricerca."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Luigi Toscano"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "luigi.toscano@tiscali.it"

#~ msgid "File Search"
#~ msgstr "Ricerca file"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Copyright 2007-2010 di Sebastian Trüg"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trüg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgid "Add folder configuration…"
#~ msgstr "Aggiungi configurazione cartella…"

#, fuzzy
#~| msgid "Folder %1 is already excluded"
#~ msgid "%1 is excluded."
#~ msgstr "La cartella %1 è già esclusa"

#~ msgid "Do not search in these locations:"
#~ msgstr "Non cercare in questi luoghi:"

#~ msgid "Select the folder which should be excluded"
#~ msgstr "Seleziona la cartella da escludere"

#~ msgid ""
#~ "Not allowed to exclude root folder, please disable File Search if you do "
#~ "not want it"
#~ msgstr ""
#~ "Non è permesso escludere la cartella radice, disabilita la ricerca nei "
#~ "file se non lo vuoi"

#~ msgid "Folder's parent %1 is already excluded"
#~ msgstr "La cartella genitore %1 è già esclusa"

#~ msgid "Configure File Search"
#~ msgstr "Configura ricerca nei file"

#~ msgid "The root directory is always hidden"
#~ msgstr "La cartella radice è sempre nascosta"
