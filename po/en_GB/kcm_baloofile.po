# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2017, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2022-12-31 17:00+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ui/main.qml:57
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."

#: ui/main.qml:66
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."

#: ui/main.qml:68
#, kde-format
msgid "Delete Index Data"
msgstr "Delete Index Data"

#: ui/main.qml:88
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr "The system must be restarted before these changes will take effect."

#: ui/main.qml:92
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Restart"

#: ui/main.qml:99
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"File Search helps you quickly locate all your files based on their content."

#: ui/main.qml:106
#, kde-format
msgid "Enable File Search"
msgstr "Enable File Search"

#: ui/main.qml:121
#, kde-format
msgid "Also index file content"
msgstr "Also index file content"

#: ui/main.qml:135
#, kde-format
msgid "Index hidden files and folders"
msgstr "Index hidden files and folders"

#: ui/main.qml:159
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Status: %1, %2% complete"

#: ui/main.qml:164
#, kde-format
msgid "Pause Indexer"
msgstr "Pause Indexer"

#: ui/main.qml:164
#, kde-format
msgid "Resume Indexer"
msgstr "Resume Indexer"

#: ui/main.qml:177
#, kde-format
msgid "Currently indexing: %1"
msgstr "Currently indexing: %1"

#: ui/main.qml:182
#, kde-format
msgid "Folder specific configuration:"
msgstr "Folder specific configuration:"

#: ui/main.qml:209
#, kde-format
msgid "Start indexing a folder…"
msgstr "Start indexing a folder…"

#: ui/main.qml:220
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Stop indexing a folder…"

#: ui/main.qml:271
#, kde-format
msgid "Not indexed"
msgstr "Not indexed"

#: ui/main.qml:272
#, kde-format
msgid "Indexed"
msgstr "Indexed"

#: ui/main.qml:302
#, kde-format
msgid "Delete entry"
msgstr "Delete entry"

#: ui/main.qml:317
#, kde-format
msgid "Select a folder to include"
msgstr "Select a folder to include"

#: ui/main.qml:317
#, kde-format
msgid "Select a folder to exclude"
msgstr "Select a folder to exclude"

#~ msgid ""
#~ "This module lets you configure the file indexer and search functionality."
#~ msgstr ""
#~ "This module lets you configure the file indexer and search functionality."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "steve.allewell@gmail.com"

#~ msgid "File Search"
#~ msgstr "File Search"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Copyright 2007-2010 Sebastian Trüg"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trüg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgid "Add folder configuration…"
#~ msgstr "Add folder configuration…"

#~ msgid "%1 is included."
#~ msgstr "%1 is included."

#~ msgid "%1 is excluded."
#~ msgstr "%1 is excluded."

#~ msgid "Disable indexing"
#~ msgstr "Disable indexing"

#~ msgid "Do not search in these locations:"
#~ msgstr "Do not search in these locations:"

#~ msgid "Select the folder which should be excluded"
#~ msgstr "Select the folder which should be excluded"

#~ msgid ""
#~ "Not allowed to exclude root folder, please disable File Search if you do "
#~ "not want it"
#~ msgstr ""
#~ "Not allowed to exclude root folder, please disable File Search if you do "
#~ "not want it"

#~ msgid "Folder's parent %1 is already excluded"
#~ msgstr "Folder's parent %1 is already excluded"

#~ msgid "Configure File Search"
#~ msgstr "Configure File Search"

#~ msgid "The root directory is always hidden"
#~ msgstr "The root directory is always hidden"
