# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-08 02:19+0000\n"
"PO-Revision-Date: 2021-06-10 12:26+0300\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 2.4.3\n"

#: ExtraActivitiesInterface.cpp:34 MainConfigurationWidget.cpp:32
#, kde-format
msgid "Activities"
msgstr "Veiklos"

#: imports/activitysettings.cpp:52
#, kde-format
msgctxt "@title:window"
msgid "Delete Activity"
msgstr "Ištrinti veiklą"

#: imports/activitysettings.cpp:52
#, kde-format
msgid "Are you sure you want to delete '%1'?"
msgstr "Ar tikrai norite ištrinti „%1“?"

#: imports/dialog.cpp:116
#, kde-format
msgid ""
"Error loading the QML files. Check your installation.\n"
"Missing %1"
msgstr ""
"Klaida įkeliant QML failus. Patikrinkite sistemoje įdiegtus paketus.\n"
"Trūksta %1"

#: imports/dialog.cpp:134
#, kde-format
msgctxt "@title:window"
msgid "Create a New Activity"
msgstr "Sukurti naują veiklą"

#: imports/dialog.cpp:135
#, kde-format
msgctxt "@title:window"
msgid "Activity Settings"
msgstr "Veiklos nuostatos"

#: imports/dialog.cpp:137
#, kde-format
msgctxt "@action:button"
msgid "Create"
msgstr "Sukurti"

#: imports/qml/activityDialog/GeneralTab.qml:43
#, kde-format
msgid "Icon:"
msgstr "Piktograma:"

#: imports/qml/activityDialog/GeneralTab.qml:57
#, kde-format
msgid "Name:"
msgstr "Pavadinimas:"

#: imports/qml/activityDialog/GeneralTab.qml:62
#, kde-format
msgid "Description:"
msgstr "Aprašas:"

#: imports/qml/activityDialog/GeneralTab.qml:71
#, kde-format
msgid "Privacy:"
msgstr "Privatumas:"

#: imports/qml/activityDialog/GeneralTab.qml:72
#, kde-format
msgid "Do not track usage for this activity"
msgstr "Nesekti šios veiklos naudojimo"

#: imports/qml/activityDialog/GeneralTab.qml:77
#, kde-format
msgid "Shortcut for switching:"
msgstr "Spartieji klavišai perjungimui:"

#. i18n: ectx: label, entry (keepHistoryFor), group (Plugin-org.kde.ActivityManager.Resources.Scoring)
#: kactivitymanagerd_plugins_settings.kcfg:10
#, kde-format
msgid "How many months keep the activity history"
msgstr "Kiek mėnesių saugoti veiklos žurnalą"

#. i18n: ectx: label, entry (whatToRemember), group (Plugin-org.kde.ActivityManager.Resources.Scoring)
#: kactivitymanagerd_plugins_settings.kcfg:17
#, kde-format
msgid "Which data to keep in activity history"
msgstr "Kuriuos duomenis laikyti veiklos žurnale"

#. i18n: ectx: label, entry (allowedApplications), group (Plugin-org.kde.ActivityManager.Resources.Scoring)
#: kactivitymanagerd_plugins_settings.kcfg:21
#, kde-format
msgid "List of Applications whose activity history to save"
msgstr "Programų, kurių veiklos istoriją įrašyti, sąrašas"

#. i18n: ectx: label, entry (blockedApplications), group (Plugin-org.kde.ActivityManager.Resources.Scoring)
#: kactivitymanagerd_plugins_settings.kcfg:24
#, kde-format
msgid "List of Applications whose activity history not to save"
msgstr "Programų, kurių veiklos istorijos neįrašyti, sąrašas"

#: MainConfigurationWidget.cpp:33
#, kde-format
msgid "Switching"
msgstr "Perjungimas"

#: qml/activitiesTab/ActivitiesView.qml:55
#, kde-format
msgctxt "@info:tooltip"
msgid "Configure %1 activity"
msgstr "Konfigūruoti %1 veiklą"

#: qml/activitiesTab/ActivitiesView.qml:62
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete %1 activity"
msgstr "Ištrinti %1 veiklą"

#: qml/activitiesTab/ActivitiesView.qml:73
#, kde-format
msgid "Create New…"
msgstr "Sukurti naują…"

#: SwitchingTab.cpp:52
#, kde-format
msgid "Activity switching"
msgstr "Persijungimas tarp veiklų"

#: SwitchingTab.cpp:55
#, kde-format
msgctxt "@action"
msgid "Walk through activities"
msgstr "Eiti per veiklas"

#: SwitchingTab.cpp:56
#, kde-format
msgctxt "@action"
msgid "Walk through activities (Reverse)"
msgstr "Eiti per veiklas (atvirkščiai)"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_virtualDesktopSwitchEnabled)
#: ui/SwitchingTabBase.ui:22
#, kde-format
msgid "Remember for each activity (needs restart)"
msgstr "Įsiminti kiekvienai veiklai (reikia paleisti iš naujo)"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: ui/SwitchingTabBase.ui:29
#, kde-format
msgid "Current virtual desktop:"
msgstr "Dabartinis virtualus darbalaukis:"

#. i18n: ectx: property (text), widget (QLabel, label_1)
#: ui/SwitchingTabBase.ui:38
#, kde-format
msgid "Shortcuts:"
msgstr "Spartieji klavišai:"

#~ msgid "General"
#~ msgstr "Bendra"

#~ msgid "Privacy"
#~ msgstr "Privatumas"

#~ msgctxt "unlimited number of months"
#~ msgid "Forever"
#~ msgstr "Amžinai"

#~ msgid "Forget the last hour"
#~ msgstr "Pamiršti apie paskutinę valandą"

#~ msgid "Forget the last two hours"
#~ msgstr "Pamiršti apie paskutines dvi valandas"

#~ msgid "Forget a day"
#~ msgstr "Pamiršti apie paskutinę dieną"

#~ msgid "Forget everything"
#~ msgstr "Viską pamiršti"

#~ msgctxt "unit of time. months to keep the history"
#~ msgid " month"
#~ msgid_plural " months"
#~ msgstr[0] " mėnesį"
#~ msgstr[1] " mėnesius"
#~ msgstr[2] " mėnesių"
#~ msgstr[3] " mėnesį"

#~ msgctxt "for in 'keep history for 5 months'"
#~ msgid "For "
#~ msgstr " "

#~ msgid "Cleared the activity history."
#~ msgstr "Veiklos žurnalas išvalytas."

#~ msgid "Keep history:"
#~ msgstr "Laikyti įrašus žurnale:"

#~ msgid "Clear History"
#~ msgstr "Išvalyti žurnalą"

#~ msgid "For a&ll applications"
#~ msgstr "&Visų programų"

#~ msgid "&Do not remember"
#~ msgstr "&Neprisiminti"

#~ msgid "O&nly for specific applications:"
#~ msgstr "&Tik nurodytų programų:"

#~ msgid "Remember opened documents:"
#~ msgstr "Prisiminti atvertus dokumentus:"

#~ msgid "Blacklist applications not on the list"
#~ msgstr "Jei programų šiame sąraše nėra, įtraukti jas į juodąjį sąrašą"

#, fuzzy
#~| msgctxt "@title:window"
#~| msgid "Delete Activity"
#~ msgctxt "@info:tooltip"
#~ msgid "Delete "
#~ msgstr "Ištrinti veiklą"

#~ msgid "Other"
#~ msgstr "Kita"

#~ msgctxt "@action:button"
#~ msgid "Apply"
#~ msgstr "Taikyti"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "Atsisakyti"

#~ msgid "Activity information"
#~ msgstr "Veiklos informacija"

#~ msgid "Wallpaper"
#~ msgstr "Darbalaukio fonas"

#~ msgctxt "@action:button"
#~ msgid "Change..."
#~ msgstr "Keisti..."

#~ msgctxt "for in 'keep history for 5 months'"
#~ msgid "for "
#~ msgstr " "
